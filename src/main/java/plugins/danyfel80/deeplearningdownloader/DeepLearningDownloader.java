package plugins.danyfel80.deeplearningdownloader;

import java.io.IOException;
import java.util.Objects;

import org.bioimageanalysis.icy.deeplearning.versionmanager.downloading.DeepLearningVersionDownloader;

import icy.common.listener.ProgressListener;
import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import io.bioimage.modelrunner.versionmanagement.DeepLearningVersion;

/**
 * This plugin handles the loading into the JVM of a SINGLE instance of the TensorFlow API for Java.
 * 
 * @author Daniel Felipe Gonzalez Obando and Carlos Garcia Lopez de Haro
 */
public class DeepLearningDownloader extends Plugin implements PluginLibrary
{

    /**
     * Downloads the {@code targetVersion} library. 
     * You can also force the download of the library even if is has already been downloaded.
     * 
     * @param targetVersion
     *        Target TensorFlow library version.
     * @param forceDownload
     *        If true, library artifacts will be downloaded even if they have already been downloaded. Otherwise, if a downloaded version of the artifacts
     *        already exist, then it will be used.
     * @param progressListener
     *        Listener handling progress events. Can be set to null.
     * @throws IOException
     *         If an error occurs during the download the library. Also, if there is an already loaded version and the given target version is
     *         different from the one already loaded.
     */
    public static synchronized void downloadLibrary(DeepLearningVersion targetVersion, boolean forceDownload,
            ProgressListener progressListener)
            throws IOException
    {
        Objects.requireNonNull(targetVersion, "Null Deep Learning version");
        targetVersion.setEnginesDir();

        try
        {
            if (!DeepLearningVersionDownloader.isDownloaded(targetVersion) || forceDownload)
            {
                if (progressListener != null)
                    DeepLearningVersionDownloader.download(targetVersion, progressListener);
                else
                    DeepLearningVersionDownloader.download(targetVersion);
            }
            else
            {
                DeepLearningVersionDownloader.getDownloadedFilePaths(targetVersion);
            }
        }
        catch (Exception e)
        {
            String message = "Error downloading " + targetVersion.folderName();
            throw new IOException(message + "\n" + e);
        }
        if (targetVersion.checkMissingJars().size() != 0) {
        	String message = "Error downloading " + targetVersion.folderName()
					+ ". Some of the required JAR files are missing. Try downloading again the engine.";
            throw new IOException(message);
        }
    }
}
