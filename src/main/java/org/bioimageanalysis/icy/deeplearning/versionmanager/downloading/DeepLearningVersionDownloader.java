package org.bioimageanalysis.icy.deeplearning.versionmanager.downloading;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.bioimageanalysis.icy.deeplearning.util.StreamUtils;

import com.google.common.util.concurrent.AtomicDouble;

import icy.common.listener.ProgressListener;
import io.bioimage.modelrunner.versionmanagement.DeepLearningVersion;
import io.bioimage.modelrunner.versionmanagement.InstalledEngines;

/**
 * Downloader for Deep Learning engine versions.
 * 
 * @author Daniel Felipe Gonzalez Obando and Carlos Garcia Lopez de Haro
 */
public class DeepLearningVersionDownloader
{
    /**
     * The path where Deep Learning libraries are downloaded.
     */
    private static Path downloadsPath = Paths.get(InstalledEngines.getEnginesDir()).toAbsolutePath();

    /**
     * Checks if the target Downloaded version is already downloaded locally.
     * 
     * @param targetVersion
     *        The TF version to be checked
     * @return {@code true} if a local version of the target library version already exists in the current system. {@code false} otherwise.
     */
    public static boolean isDownloaded(DeepLearningVersion targetVersion)
    {
        Path versionDirectory = getVersionDirectoryPath(targetVersion);
        boolean directoryExists = Files.exists(versionDirectory) && Files.isDirectory(versionDirectory);
        if (directoryExists)
        {
            for (String jarUrl : targetVersion.getJars())
            {
                Path localPath;
                try
                {
                    localPath = toLocalVersionFile(versionDirectory, jarUrl);
                }
                catch (MalformedURLException e)
                {
                    e.printStackTrace();
                    return false;
                }
                if (Files.notExists(localPath))
                {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Retrieves the download path for the given TensorFlow version.
     * 
     * @param targetVersion
     *        The target TF version.
     * @return Download path for target version.
     */
    private static Path getVersionDirectoryPath(DeepLearningVersion targetVersion)
    {
        return downloadsPath.resolve(targetVersion.folderName()).toAbsolutePath();
    }
    
    /**
     * Get the path where all the Deep Learning engines are stored
     * @return the path where all the Deep Learning engines are stored
     */
    public static String getEnginesFolderPathAsString() {
    	return downloadsPath.toString();
    }
    
    /**
     * Get the path where all the Deep Learning engines are stored
     * @return the path where all the Deep Learning engines are stored
     */
    public static Path getEnginesFolderPathAsPath() {
    	return downloadsPath;
    }
    
    /**
     * Get String array of engine folders in the engines folder
     * @return string array with folder names inside the engines folder
     */
    public static String[] getEnginePaths() {
    	if (!downloadsPath.toFile().exists())
    		return new String[0];
    	return downloadsPath.toFile().list();
    }

    /**
     * Takes the filename of the provided URL and resolves it on the provided version directory.
     * 
     * @param versionDirectory
     *        The version directory path to resolve the local version.
     * @param urlString
     *        The URL of the target file.
     * @return The path in the versionDirectory resolving the file in the given URL.
     * @throws MalformedURLException
     *         If the URL cannot be correctly parsed.
     */
    private static Path toLocalVersionFile(Path versionDirectory, String urlString) throws MalformedURLException
    {
        URL url = new URL(urlString);
        Path fileName = Paths.get(url.getPath()).getFileName();
        return versionDirectory.resolve(fileName);
    }

    /**
     * Given a target TF version, files related to it are checked to see if they already exist locally or else they are downloaded to the local directory.
     * 
     * @param targetVersion
     *        The target version to download.
     * @return The list of paths for all resources downloaded or already existing in local storage.
     * @throws IOException
     *         If an error occurs while files are checked or downloaded.
     */
    public static List<Path> download(DeepLearningVersion targetVersion) throws IOException
    {
        Path versionDirectory = getVersionDirectoryPath(targetVersion);
        if (Files.notExists(versionDirectory))
        {
            Files.createDirectories(versionDirectory);
        }
        ForkJoinPool pool = new ForkJoinPool();
        Future<List<Path>> resultList = pool.submit(() -> {
            final AtomicInteger downloadedFiles = new AtomicInteger();
            final int totalFiles = targetVersion.getJars().size();
            return targetVersion.getJars().parallelStream().map(urlString -> {
                try
                {
                    Path resultPath = downloadFile(urlString, versionDirectory);
                    System.out.println("(" + downloadedFiles.incrementAndGet() + "/" + (totalFiles)
                            + ") File downloaded to: " + resultPath.toString());
                    return resultPath;
                }
                catch (IOException e)
                {
                    System.out.println("(" + downloadedFiles.incrementAndGet() + "/" + (totalFiles)
                            + ") File failed downloading: " + urlString);
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList());
        });
        try
        {
            return resultList.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            throw new IOException(e);
        }
    }

    /**
     * Given a target TF version, files related to it are checked to see if they already exist locally or else they are downloaded to the local directory.
     * 
     * @param targetVersion
     *        The target version to download.
     * @param progressListener
     *        The listener handling progress events.
     * @return The list of paths for all resources downloaded or already existing in local storage.
     * @throws IOException
     *         If an error occurs while files are checked or downloaded.
     */
    public static List<Path> download(DeepLearningVersion targetVersion, ProgressListener progressListener)
            throws IOException
    {
        Path versionDirectory = getVersionDirectoryPath(targetVersion);
        if (Files.notExists(versionDirectory))
        {
            Files.createDirectories(versionDirectory);
        }
        ForkJoinPool pool = new ForkJoinPool();
        Future<List<Path>> resultList = pool.submit(() -> {
            List<String> jarUrlStrings = targetVersion.getJars();
            final int totalFiles = jarUrlStrings.size();
            final AtomicDouble[] filesProgress = IntStream.range(0, totalFiles)
                    .mapToObj(i -> new AtomicDouble(0))
                    .toArray(AtomicDouble[]::new);
            return IntStream.range(0, totalFiles).parallel().boxed()
                    .map(StreamUtils.wrapFunction(i -> {
                        String jarUrlString = jarUrlStrings.get(i);
                        ProgressListener l = (progress, total) -> {
                            filesProgress[i].set(progress / total);
                            double currentProgress = Arrays.stream(filesProgress).mapToDouble(fp -> fp.get()).sum();
                            progressListener.notifyProgress(currentProgress, totalFiles);
                            return false;
                        };
                        try
                        {
                            return downloadFile(jarUrlString, versionDirectory, l);
                        }
                        catch (IOException e)
                        {
                            throw new RuntimeException("File failed downloading:" + jarUrlString, e);
                        }
                    }))
                    .collect(Collectors.toList());
        });
        try
        {
            return resultList.get();
        }
        catch (InterruptedException | ExecutionException e)
        {
            throw new IOException(e);
        }
    }

    private static Path downloadFile(String urlString, Path versionDirectory) throws IOException
    {
        URL url = new URL(urlString);
        Path localFilePath = toLocalVersionFile(versionDirectory, urlString);
        Files.deleteIfExists(localFilePath);

        ReadableByteChannel readableByteChannel = Channels.newChannel(url.openStream());
        try (FileOutputStream fileOutputStream = new FileOutputStream(localFilePath.toString()))
        {
            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        }

        return localFilePath;
    }

    private static Path downloadFile(String urlString, Path targetDirectory, ProgressListener progressListener)
            throws IOException
    {
        URL url = new URL(urlString);
        Path localFilePath = toLocalVersionFile(targetDirectory, urlString);
        Files.deleteIfExists(localFilePath);

        try
        {
            HttpURLConnection httpConnection = (HttpURLConnection) (url.openConnection());
            while (httpConnection.getResponseCode() == 302) {
            	// TODO use method from JDLL
            	url = redirectedURL(url, httpConnection);
            	httpConnection = (HttpURLConnection) (url.openConnection());
            }
            long completeFileSize = httpConnection.getContentLengthLong();
            if (completeFileSize == -1) {
            	progressListener.notifyProgress(1, 1);
            	return downloadFile(urlString, targetDirectory);
            }
            try (BufferedInputStream in = new BufferedInputStream(httpConnection.getInputStream()))
            {
                FileOutputStream fos = new FileOutputStream(localFilePath.toFile());
                try (BufferedOutputStream bout = new BufferedOutputStream(fos, 1024))
                {
                    byte[] data = new byte[1024];
                    long downloadedFileSize = 0;
                    int x = 0;

                    while ((x = in.read(data, 0, 1024)) >= 0)
                    {
                        downloadedFileSize += x;
                        bout.write(data, 0, x);

                        // notify progress
                        progressListener.notifyProgress(downloadedFileSize, completeFileSize);
                    }
                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return localFilePath;
    }
    
    /**
     * TODO replae and use method from JDLL
     * @param url
     * @param conn
     * @return
     */
	public static URL redirectedURL(URL url, HttpURLConnection conn) {
		int statusCode;
		try {
			statusCode = conn.getResponseCode();
		} catch (IOException ex) {
			return url;
		}
		if (statusCode != HttpURLConnection.HTTP_MOVED_TEMP
            && statusCode != HttpURLConnection.HTTP_MOVED_PERM
            && statusCode != HttpURLConnection.HTTP_SEE_OTHER)
			return url;
		String newURL = conn.getHeaderField("Location");
		try {
			return new URL(newURL);
		} catch (MalformedURLException ex) {
		}
        try {
        	URI uri = url.toURI();
            String scheme = uri.getScheme();
            String host = uri.getHost();
            String mainDomain = scheme + "://" + host;
			return new URL(mainDomain + newURL);
		} catch (URISyntaxException | MalformedURLException e) {
			return null;
		}
	}

    /**
     * Retrieves the paths of all resources associated to a given version resolved locally.
     * 
     * @param targetVersion
     *        The target TF version.
     * @return List of paths of all resources associated to a given version resolved locally.
     */
    public static List<Path> getDownloadedFilePaths(DeepLearningVersion targetVersion)
    {
        Path versionDirectory = getVersionDirectoryPath(targetVersion);
        List<Path> pathList = targetVersion.getJars().stream().map(urlString -> {
            try
            {
                return toLocalVersionFile(versionDirectory, urlString);
            }
            catch (MalformedURLException e)
            {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());
        return pathList;
    }

}
